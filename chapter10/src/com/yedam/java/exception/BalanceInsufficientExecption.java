package com.yedam.java.exception;

public class BalanceInsufficientExecption extends Exception{
	//사용자 정의 예외 -> 예외 클래스 상속받음 됨
	
	public BalanceInsufficientExecption() {}
	public BalanceInsufficientExecption(String message) {
		super(message);
	}
}
