package com.yedam.java.exception;

public class Account {
	private long balance;
	
	public long getBalance() {
		return this.balance;
	}
	
	//입금(입력한 값에 잔액 더해야 해서 입력받은 값 바로 넣는 setter사용 노)
	public void deposit(int money) { 
		this.balance += money; 
	}
	
	//출금
	public void withDraw(int money) throws BalanceInsufficientExecption { //메소드 호출하는 곳에서 예외처리해달라 떠넘기는것
		if(this.balance < money) {
			throw new BalanceInsufficientExecption("잔고부족: "+ (money - balance)+ "이 모자랍니다.");
			//throw => 내가 임의로 오류를 집어넣는것.
			//throw쓰는것과 return(메소드 종료)쓰는거랑 메소드 입장에서는 같다. -> else안써줘도됨
		}
		balance -= money;
	}
}

