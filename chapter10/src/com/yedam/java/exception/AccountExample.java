package com.yedam.java.exception;

public class AccountExample {
	public static void main(String[] args) {
		Account account = new Account();
		
		//예금
		account.deposit(10000);
		System.out.println("잔액: "+account.getBalance());
		
		//출금
		try {
		account.withDraw(30000);
		}catch(BalanceInsufficientExecption e) {
			System.out.println("메세지=> "+ e.getMessage());
			e.printStackTrace();
		}
	}
}

