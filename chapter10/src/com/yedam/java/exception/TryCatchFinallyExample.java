package com.yedam.java.exception;

public class TryCatchFinallyExample {
	public static void main(String[] args) {
		try {

			String data1 = args[0];
			String data2 = args[1];
			
			int value1 = Integer.parseInt(data1);
			int value2 = Integer.parseInt(data2);
			
			int result1 = value1 + value2;
			System.out.println(data1 + "+" + data2 + '=' + result1);
			
			//예외로 강제로 발생
			//throw new NullPointerException();
			
		//캐치구문에 있는 2개 이외의 예외는 처리 못함. => 개별적으로 할땐 일반적으로 끝에 exception 추가
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("실행 시키는 경우 값이 1개 이상 필요합니다.");
			
		}catch(NumberFormatException e) {
			System.out.println("실행 시키는 경우 숫자가 입력되어야 합니다.");
			
		}catch(Exception e) {
			//위에서 정의하지 못한 예외 처리하겠다.
			System.out.println("기타 예외가 발생했습니다.");
			
		}finally {
			//finally는 무조건 거쳐야 한다. (system.exit()써도 실행)
			System.out.println("다시 실행하세요.");
			
		}
		
		//throws를 사용했기때문에 예외 처리 해야한다. 
		//(but 메인에 떠넘기기 하면 사실상 처리 안하겠다 하는거랑 같음)
		try {
			findClass();
			
		}catch(ClassNotFoundException e) {
			System.out.println(e.getMessage()); // 순수하게 메세지만 가지고 오는것.
			System.out.println("========================");
			e.printStackTrace(); //디폴트
		}
		
		
	}

	public static void findClass() throws ClassNotFoundException { 
		Class clazz = Class.forName("java.lang.String2"); // 예외처리를 요구하는 메소드
		//forname메소드 자체에 throws가 있다. (throws는 실행예외든 일반예외든 상관없이 처리해야한다.) -> 그래서 빨간줄 
		//try catch 문을 쓰거나, 메소드로 쓰려고 하는 경우 떠넘기기 가능
		
	}
}

