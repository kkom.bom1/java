package com.yedam.java.ch01;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;


public class NonBufferVSBufferExample {
	public static void main(String[] args) throws Exception{
		//buffer(보조스트림)가 없을때 
		//경로(해당클래스가 존재하는 패키지 내 파일의 경로 받아옴)
		String originalFilePath1 
		= NonBufferVSBufferExample.class.getResource("kkomi.jpg").getPath();
		
		//어디에 복사할지 경로 지정
		String targetFilePath1 = "C:/Temp/targetFile1.jpg";
		InputStream fis1 = new FileInputStream(originalFilePath1);
		OutputStream fos1 = new FileOutputStream(targetFilePath1);
		
		long nonBufferTime = copy(fis1, fos1);
		System.out.println("버퍼를 사용하지 않았을 때 "+ nonBufferTime + "ns");
		System.out.println();
		
		fis1.close();
		fos1.close();
		
		//=======================================================================================
		//buffer(보조스트림) 사용할떄   -?????????????
		String originalFilePath2 
		= NonBufferVSBufferExample.class.getResource("kkomi.jpg").getPath();
		String targetFilePath2 = "C:/Temp/targetFile1.jpg";
		InputStream fis2 = new FileInputStream(originalFilePath2);
		OutputStream fos2 = new FileOutputStream(targetFilePath2);
		
		BufferedInputStream bis = new BufferedInputStream(fis2); 
		//DataInputStream dis = new DataInputStream(bis);
		BufferedOutputStream bos = new BufferedOutputStream(fos2);
		
		long bufferTime = copy(bis, bos);
		System.out.println("버퍼를 사용했을때 "+ bufferTime + "ns");
		
		bis.close();
		bos.close();
	
		
	}
	
	//copy메소드
	public static long copy(InputStream is, OutputStream os) throws Exception {
		long start = System.nanoTime();
		while(true) {
			//읽자마자 바로 파일 만들기
			int data = is.read();
			if(data == -1) break;
			os.write(data);
		}
		os.flush();
		long end = System.nanoTime();
		return (end - start);
	}
}
