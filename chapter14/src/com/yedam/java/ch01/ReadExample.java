package com.yedam.java.ch01;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;

public class ReadExample {
	public static void main(String[] args) throws Exception {
		//바이트 타입
		InputStream is = new FileInputStream("C:/Temp/test1.db");
		
		//read는 바이트 타입이 아니라 int타입을 반환한다.
		//read는 한글자씩 읽는다. -> 전부출력되지 않음
//		int data = is.read();
//		System.out.println(data);
		
		//전부읽어들이고 싶을때 
		//1)while문을 돌린다.(한글자씩 가져옴)
		while(true) {
			int data = is.read();
			if(data == -1) break;   //(int) data 값이 없으면 -1을 반환한다. 
			System.out.println(data);
		}
		System.out.println();
		is.close();
		
		is = new FileInputStream("C:/Temp/test1.db"); //다시 읽고싶음 스트림을 닫았다가 다시 열어야함ㄴ
		//2)buffer(배열사용) -> 매개변수로 배열을 넘겨서 배열에 값을 담아서 그 배열을 출력 (한번에 가져옴)
		byte[] buffer = new byte[100];
		while(true) {
			int readByteNum = is.read(buffer);   // int readByteNum 변수 -> 몇글자 읽었는가 알려주는 역할(배열에 몇개의 값이 담겨있는지 알려주는 값)
			if(readByteNum == -1) break; //배열에 암것도 안담겨 있음 -1반환
			for(int i=0; i<readByteNum; i++) {  //배열이 값이 다 안담겼을수 있으니 readByteNum을 이용해서 최대 반복횟수지정
				System.out.println(buffer[i]);
			}
		}
		
		is.close();
		
		
		//====================================================================================================
		System.out.println();
		//문자 타입
		//read는 char타입이 아니라 int타입을 반환한다./ 한글자만 읽어들인다.
		//1) while 사용
		Reader reader = new FileReader("C:/Temp/test2.txt");
		while(true) {
		int data = reader.read();
		if(data == -1) break;
		System.out.print((char)data);
		}
		reader.close();
		System.out.println();
		
		//2)buffer(배열사용)
		reader = new FileReader("C:/Temp/test2.txt");
		
		char[] charBuffer = new char[100];
		while(true) {
			int readCharNum = reader.read(charBuffer);
			if(readCharNum == -1) break;
			for(int i=0; i< readCharNum; i++) {
				System.out.print(charBuffer[i]);
			}
		}
		reader.close();
		
		
		
	}
}
