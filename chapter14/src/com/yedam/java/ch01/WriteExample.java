package com.yedam.java.ch01;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;

public class WriteExample {
	public static void main(String[] args) throws Exception {
		//파일내보내기
		//바이트 기반 출력 스트림
		OutputStream os = new FileOutputStream("C:/Temp/test1.db");
		 
		byte a = 10;
		byte b = 20;
		byte c = 30;
		
		os.write(a);
		os.write(b);
		os.write(c);
		
		byte[] byteAray = { 40, 50, 60 };
		os.write(byteAray);
		//os.write(byteAray, 2, 1); -> 두번째부터 2개만 보내겠다.
		
		os.flush();
		os.close();
		
		
		//문자 기반 출력 스트림
		Writer writer = new FileWriter("C:/Temp/test2.txt");
		
		char ca = 'A';
		char cb = 'B';
		
		writer.write(ca);
		writer.write(cb);
		
		char[] charAray = {'c','D','E'};
		writer.write(charAray, 1, 2);
		
		String str = "\nHello World!";
		writer.write(str);
		
		writer.flush(); //마무리 짓고 싶을때, close할때 사용
		writer.close();
		
		
		
	}
}
