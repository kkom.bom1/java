package com.yedam.java.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectExample {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//1. JDBC 드라이버 연결
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		//2. DB에 서버 접속
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String id = "hr";
		String password = "hr";
		
		Connection conn = DriverManager.getConnection(url, id, password);
		
		//3. Statement/PreparedStatement 객체 생성
		Statement stmt = conn.createStatement();
		
		//4. SQL실행
		String sql = "SELECT * FROM employees";
		ResultSet rs = stmt.executeQuery(sql);
		
		//5. 결과값 출력
		while(rs.next()) {
			String name = "이름: "+ rs.getString("first_name") + " " + rs.getString("last_name");
			System.out.println(name);
		}
		
		//6. 자원 해제하기
		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
		if(conn != null) conn.close();
		

	}

}
