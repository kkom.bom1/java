package com.yedam.java.app; 

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import com.yedam.java.emp.EmpDAO;
import com.yedam.java.emp.EmpDAOImpl;
import com.yedam.java.emp.EmpVO;

public class EmpManagement {
	Scanner sc = new Scanner(System.in);
	EmpDAO empDAO = EmpDAOImpl.getInstance();
	
	public EmpManagement() {
		while(true) {
			//메뉴출력
			menuPrint();
			//메뉴선택
			int menuNo = menuSelect();
			//각메뉴의 기능을 실행
			if(menuNo ==1) {
				//전체조회
				selectAll();
			}else if(menuNo ==2) {
				//단건조회
				selectOne();
			}else if(menuNo ==3) {
				//사원등록
				insertEmp();
			}else if(menuNo ==4) {
				//사원수정
				updateEmp();
			}else if(menuNo ==5) {
				//사줭 삭제
				deleteEmp();
			}else if(menuNo ==9) {
				//종료
				end();                     ///찐으로 프로그램 종료하는건 안만드나??
			}else {
				//기타사항
				printErrorMessage();
				
			}
		}
	}
	
	private void menuPrint() {
		System.out.println("==================================================");
		System.out.println("1.전체조회 2.사원조회 3.사원등록 4.사원수정 5.사원삭제 9.종료");
		System.out.println("==================================================");
	}
	
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어 있습니다.");
		}
		return menuNo;
	}
	
	private void selectAll() {
		List<EmpVO> list = empDAO.selectAll();
		
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		
		for(EmpVO empVO : list) {
			System.out.printf("%d: %s, %s, \n", 
					empVO.getEmployeeId(), empVO.getFirstName(), empVO.getLastName());
		}
	}
	
	private void selectOne() {
		EmpVO findEmp = inputEmpInfo();
		EmpVO empVO = empDAO.selectEmpInfo(findEmp);
		if(empVO == null) {
			System.out.printf("%d 사원은 존재하지 않습니다.\n", findEmp.getEmployeeId());
		}else {
			System.out.println("검색결과> ");
			System.out.println(empVO);
		}
	}
	
	private void insertEmp() {
		EmpVO empVO = inputEmpAll();
		empDAO.insertEmpInfo(empVO);
	}
	
	private void updateEmp() {
		EmpVO empVO = inputEmpInfo();
		empDAO.updateEmpInfo(empVO);
	}
	
	private void deleteEmp()	{
		EmpVO empVO = new EmpVO();
		empVO.setEmployeeId(inputEmpNo());
		empDAO.deleteEmpInfo(empVO);
	}
	
	private EmpVO inputEmpAll()	{
		EmpVO empVO = new EmpVO();
		System.out.print("사원번호 > ");
		empVO.setEmployeeId(Integer.parseInt(sc.nextLine()));
		System.out.print("이름 > ");
		empVO.setFirstName(sc.nextLine());
		System.out.print("성 > ");
		empVO.setLastName(sc.nextLine());
		System.out.print("이메일 > ");
		empVO.setEmail(sc.nextLine());
		System.out.print("연락처 > ");
		empVO.setPhoneNumber(sc.nextLine());
		System.out.print("입사일(yyyy-MM-dd) > ");
		empVO.setHireDate(Date.valueOf(sc.nextLine()));
		System.out.print("업무 > ");
		empVO.setJobId(sc.nextLine());
		System.out.print("급여 > ");
		empVO.setSalary(Double.parseDouble(sc.nextLine()));
		System.out.print("커미션 > ");
		empVO.setCommissionPct(Double.parseDouble(sc.nextLine()));
		System.out.print("매니저 > ");
		empVO.setManagerId(Integer.parseInt(sc.nextLine()));
		System.out.print("부서 > ");
		empVO.setDepartmentId(Integer.parseInt(sc.nextLine()));
		return empVO;
		
	}
	
	private EmpVO inputEmpInfo()	{
		EmpVO empVO = new EmpVO();
		System.out.println("사원번호> ");
		empVO.setEmployeeId(Integer.parseInt(sc.nextLine()));
		System.out.println("이름> ");
		empVO.setFirstName(sc.nextLine());
		return empVO;
		
	}
	
	private int inputEmpNo() {
		int empNo = 0;
		try {
			empNo = Integer.parseInt(sc.nextLine());
			
		}catch(NumberFormatException e) {
			System.out.println("사원번호는 숫자로 구성되어 있습니다.");
		}
		return empNo;
	}
	
	
	private void printErrorMessage() {
		System.out.println("======================");
		System.out.println("메뉴를 잘못 입력하였습니다.");
		System.out.println("메뉴를 다시 확인해주세요");
		System.out.println("======================");
	}
	
	private void end() {
		System.out.println("======================");
		System.out.println("프로그램을 종료합니다.");
		System.out.println("======================");
	}
	
}
