package com.yedam.java.emp;

import java.util.ArrayList;
import java.util.List;

import com.yedam.java.common.DAO;

public class EmpDAOImpl extends DAO implements EmpDAO {
	//싱글톤 - 다른곳에서 새롭게 생성자로 객체 여러개 만들지 않고 메소드 호출 형태로 단 하나만 생성
	//(다른곳에서 하나 더 생성해서 DB에 접근하려 하면 자바에서 먼저 막아줌)
	private static EmpDAO instance = null;
	private EmpDAOImpl() {}  //다른곳에서 객체생성 못하게 생성자 호출막고 
	public static EmpDAO getInstance(){ //이걸로 한번 최초 호출해서 만들어지면 계속이것만 쓴다
		if(instance == null) 
			instance = new EmpDAOImpl();
			return instance;
	}
	
	@Override
	public List<EmpVO> selectAll() {
		List<EmpVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM employees";
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				EmpVO empVo = new EmpVO();
				empVo.setEmployeeId(rs.getInt("employee_id"));
				empVo.setFirstName(rs.getString("first_name"));
				empVo.setLastName(rs.getString("last_name"));
				empVo.setEmail(rs.getString("email"));
				empVo.setPhoneNumber(rs.getString("phone_number"));
				empVo.setHireDate(rs.getDate("hire_date"));
				empVo.setJobId(rs.getString("job_id"));
				empVo.setSalary(rs.getDouble("salary"));
				empVo.setCommissionPct(rs.getDouble("commission_pct"));
				empVo.setManagerId(rs.getInt("manager_id"));
				empVo.setDepartmentId(rs.getInt("department_id"));
				list.add(empVo);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}

	@Override
	public EmpVO selectEmpInfo(EmpVO empVO) {
		EmpVO findEmp = null;
		try {
			connect();
			//비어있는 공간 만들고
			stmt = conn.createStatement(); 
			//별도의 SQL문을 만든다
			String sal = "SELECT * FROM WHERE employee_id = '" + empVO.getEmployeeId() + "'";
			//실행할때 sql문 요구(preparedStatement는 만들때 sql문 요구 -> 실행할땐 요구 ㄴ)
			rs = stmt.executeQuery(sal);
			
			if(rs.next()) {
				findEmp = new EmpVO(); //단건조회 있을때 내용이 있을때만 인스턴스가 형성된다.
				findEmp.setEmployeeId(rs.getInt("employee_id"));
				findEmp.setFirstName(rs.getString("first_name"));
				findEmp.setLastName(rs.getString("last_name"));
				findEmp.setEmail(rs.getString("email"));
				findEmp.setPhoneNumber(rs.getString("phone_number"));
				findEmp.setHireDate(rs.getDate("hire_date"));
				findEmp.setJobId(rs.getString("job_id"));
				findEmp.setSalary(rs.getDouble("salary"));
				findEmp.setCommissionPct(rs.getDouble("commission_pct"));
				findEmp.setManagerId(rs.getInt("manager_id"));
				findEmp.setDepartmentId(rs.getInt("department_id"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return findEmp;
	}

	@Override
	public void insertEmpInfo(EmpVO empVO) {
		try {
			connect();
			
			String sql = "INSERT INTO employees VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, empVO.getEmployeeId());
			pstmt.setString(2,empVO.getFirstName());
			pstmt.setString(3,empVO.getLastName());
			pstmt.setString(4,empVO.getEmail());
			pstmt.setString(5,empVO.getPhoneNumber());
			pstmt.setDate(6, empVO.getHireDate());
			pstmt.setString(7,empVO.getJobId());
			pstmt.setDouble(8,empVO.getSalary());
			pstmt.setDouble(9,empVO.getCommissionPct());
			pstmt.setInt(10, empVO.getManagerId());
			pstmt.setInt(11, empVO.getDepartmentId());
			
			int result = pstmt.executeUpdate();
			
			if(result >0) {
				System.out.println("정상적으로 등록되었습니다.");
			}else {
				System.out.println("정상적으로 등록되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}

	@Override
	public void updateEmpInfo(EmpVO empVO) {
		try {
			connect();
			
			String sql = "UPDATE employees SET first_name WHERE employee_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, empVO.getFirstName());
			pstmt.setInt(2, empVO.getEmployeeId());
			
			int result = pstmt.executeUpdate();
			
			if(result >0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
	}

	@Override
	public void deleteEmpInfo(EmpVO empVO) {
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "DELETE FROM employees WHERE employee_id= "+ empVO.getEmployeeId();
			int result = stmt.executeUpdate(sql);
			
			if(result >0) {
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
	}



}
