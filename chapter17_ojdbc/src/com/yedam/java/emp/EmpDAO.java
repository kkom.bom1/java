package com.yedam.java.emp;

import java.util.List;

public interface EmpDAO {
	//전체조회
	public List<EmpVO> selectAll();
	
	//단건조회
	public EmpVO selectEmpInfo(EmpVO empVO);
	
	//등록
	public void insertEmpInfo (EmpVO empVO);
	
	//수정
	public void updateEmpInfo (EmpVO empVO);
	
	//삭제
	public void deleteEmpInfo (EmpVO empVO);
}
