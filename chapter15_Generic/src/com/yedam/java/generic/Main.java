package com.yedam.java.generic;

public class Main {
	public static void main(String[] args) {
		Box<String>	box1 = new Box<String>();
		
		box1.set("홍길동"); // T field = String "홍길동" (T가 STring이 됨)
		// box1.set(1000); -> 제너럴을 쓰면 사용할때 쓴 타입 이외에는 못씀.=> 실행하기 전에 오류를 잡아줌
		String name = box1.get(); // 강제타입변환할 필요 없다.(불필요한 캐스팅() 작업 생략 => 캐스팅()는 굳이 사용 ㄴ)
		
		//다른 타입 넣고 싶을때 새로운 변수 만들어줘야한다.
		//제네릭은 기본타입은 사용 할수 없다(intX -> 대용되는 (변환)클래스?? 쓰면 된다. Integer사용)
		Box<Integer> box2 = new Box<>();
		box2.set(1000);
		int value = box2.get();
		
		//제네릭 있는 메소드 사용
		Box<Apple> appleBox = Utill.boxing(new Apple());
		Box<String> nameBox = Utill.boxing("신용권");
	
	}
}
