package com.yedam.java.generic;

public class Box<T> {
	//T => 타입을 담을 수 있는 변수/ 제네릭 타입의 이름은 의미없는 대문자 아무거나
	//필요하다면 여러개 사용 가능(보통 1개 ~ 3개 <L,K,A>)
	
	private T field;
//	private K k;
	
	public void set(T value) {
		this.field = value;
	}

	public T get() {
		return this.field;
	}
}
