package com.yedam.java.generic;

public class Utill {
	
	//제네릭 타입이 사용되는 메소드
	public static <T> Box<T> boxing(T t){ 
		//        === 제네릭 T 쓰겠다고 알려줌
		Box<T> box = new Box<T>();
		box.set(t);
		return box;
	}
}
