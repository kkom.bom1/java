package com.yedam.java.common;

public class Main {
	
	public static void main(String[] args) {
		Box box = new Box();
		
		box.set("홍길동"); //자동타입변환(Object object = String "홍길동")
		box.set(10000); // => 불러오는 타입말고 다른 타입을 넣어도 오류를 보여주지 않는다.
		String name = (String)box.get(); //강제타입변환
		
		box.set(new Apple());
		Apple apple = (Apple)box.get();
	}
}
