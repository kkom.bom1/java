package com.yedam.java.common;

public class Box {
	//모든 클래스의 부모격인 Object
	private Object object;
	
	public void set(Object object) {
		this.object = object;
	}
	
	public Object get() {
		return this.object;
	}
}
