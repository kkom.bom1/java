package com.yedam.java.lambda;

public class MainB {
	public static void main(String[] args) {
		MyInterfaceB fib = (x, y) -> { int result = x + y;
		                               return result; };
		System.out.println(fib.method(4, 9));
		
		fib = (x,y) -> { return x + y; };  //return구문 한줄은 {} 생략 ㄴㄴ
		
		fib = (x,y) -> x + y; // = { return x + y; };
		
		fib = (x,y) -> sum(x,y); //= {return sum(x, y);};
	}
	
	public static int sum(int x, int y) {
		return x + y;
	}
}
