package com.yedam.java.lambda;

@FunctionalInterface
public interface MyInterfaceB {
	public int method(int x, int y);
}
