package com.yedam.java.lambda;

public class MainA {
	public static void main(String[] args) {
		MyInterfaceA fia = (a) -> {int result = a + 100;
		                           System.out.println("100을 더한 결과: " + result);};
	    fia.method(120);
	    
	    fia = a -> System.out.println("100을 더한 결과 두번째: "+ (a+100));
	    fia.method(220);
	}
}
