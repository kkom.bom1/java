package com.yedam.java.lambda;

@FunctionalInterface
public interface MyInterfaceA {
	public void method(int x);
}
