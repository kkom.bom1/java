package com.yedam.java.lambda;

@FunctionalInterface  //==> 람다식을 쓸거라고 제한을 건다. -> 하나의 메소드만 가짐
public interface MyFuncInterface { 
	public void method();
	//public void method2(); -> 오류
}
