package com.yedam.java.lambda;

public class Main {
	public static void main(String[] args) {
//      =================== -> 람다식만 썻을때 뭐가 먼지 모르니까 
		MyFuncInterface fi1 = () -> {System.out.println("람다식 메소드 구현");};
		fi1.method();
		
		
		//익명구현객체를 간단히 한것이 람다식
		MyFuncInterface fi2 = new MyFuncInterface() {
			@Override
			public void method() {
				System.out.println("익명 구현 객체 메소드 구현");
			}		
		};		
		fi2.method();
		
		
		fi1 = () -> System.out.println("람다식 메소드 구현 시 실행구문 생략");
		fi1.method();
		
		
	}
}
