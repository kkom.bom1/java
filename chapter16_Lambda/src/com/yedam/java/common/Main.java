package com.yedam.java.common;

public class Main {
	public static void main(String[] args) {
		//정석정인 인터페이스 추상메소드 사용
		MyFunctionalInterface mfi1 = new MyFunctionalClass();
		mfi1.method();
		
		//구현 클래스 내의 본인 것 사용
		MyFunctionalClass mic1 = new MyFunctionalClass();
		mic1.func();
		
//=================================================================================================		
		//익명 구현 객체(chapter9 -> 중첩인터페이스쪽) : 이름이 없어서 대신 내가 구현할 인터페이스 사용
		// 1) 상속 혹은 구현의 관계에서 가능
		// 2) 재사용 불가 재사용되지 않는 클래스에서 임시적으로 사용
		// 3) 고유의 메소드 가질수는 있으나 외부에서 호출 불가 (이름이 없으니까)
		//     cf) 인터페이스 구현 클래스 (MyFunctionalClass) -> 자신의 메소드 만들수 있다.(내것이 있음)
		
		//클래스타입 만들어보지 않고 인터페이스 구현 클래스 만들어보겠다. 
		MyFunctionalInterface mfi2 = new MyFunctionalInterface() {
//                                   =========================== -> 인터페이스에 구현객체 만들겠다.
			@Override
			public void method() {
				System.out.println("익명 구현 객체를 통해 실행");
				func(); // 별로 사용 ㄴㄴ
				
			}
			
			//자체 클래스 이름이 없어서 본인의 메소드 따로 사용 X
			public void func() {
				System.out.println("익명 구현 객체 소유 메소드");
			}
			
		};
		
		mfi2.method();
		
		
		//익명구현 객체) 부모클래스 기반 상속 클래스를 만들수도 있다. => 그대신 부모 클래스 메소드 오버라이딩 해야함 (본인것 사용 못하니까)
		MyFunctionalClass mfc2 = new MyFunctionalClass() {
			@Override
			public void method() {
				System.out.println("부모클래스 상속 후 오버라이딩");

			}
			
			@Override
			public void func() {
				System.out.println("부모클래스 상속 후 오버라이딩222");
			}
		};
		
		mfc2.method();
		mfc2.func();
		
	}
}
